<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<spring:url value="/resources" var="urlPublic" />
<spring:url value="/" var="url" />

<!doctype html>
<html>
<head>
<!-- Required meta tags -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<title>Shop Online</title>
</head>
<body>


	<jsp:include page="includes/header.jsp"></jsp:include>
	<jsp:include page="includes/menu.jsp"></jsp:include>


	<div class="container" style="margin-top: 30px">
		<div class="row">
			<div class="col-sm-4">
				<h2>Enlaces</h2>
				<h5>
					<a>Gitbook</a>
				</h5>
				<h5>
					<a>Bootstrap</a>
				</h5>
				<h5>
					<a>Spring</a>
				</h5>

			</div>
			<div class="col-sm-8">
				<h2>Bienvenido a Shop Online</h2>
				<p>Mi variable URL es ${ urlPublic }</p>

				<img src="${urlPublic}/images/logo.jpg">

			</div>
		</div>
	</div>
	<jsp:include page="includes/footer.jsp"></jsp:include>


</body>
</html>
