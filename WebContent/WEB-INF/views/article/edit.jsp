<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>    
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<spring:url value="/" var="urlPublic" />   
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style>
table, th, td {
   border: 1px solid black;
}
</style>
<title>Insert title here</title>
</head>
<body>

<h1>Edición de artículos</h1>

${ msg } 

<form method=post action="${ urlPublic}/articles/${ id }" >
	<label>Código</label>
	<input value=${ code }><br>
	<label>Nombre</label>
	<input value=${ name }><br>
	<label>Precio</label>
	<input value=${ price }><br>

	<input type="submit" value="Enviar"><br>
</form>

</body>
</html>