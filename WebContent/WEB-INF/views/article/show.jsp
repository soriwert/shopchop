<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>    
<spring:url value="/resources" var="urlPublic" />
<spring:url value="/" var="url" />

<!doctype html>
<html>
<head>
<!-- Required meta tags -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<title>Shop Online</title>
</head>
<body>

	<jsp:include page="../includes/header.jsp"></jsp:include>
	<jsp:include page="../includes/menu.jsp"></jsp:include>

	<div class="container" style="margin-top: 30px">
		<div class="row">
			<div class="col-sm-12">
				<h1>Shop Online</h1>
				<h2>Artículo</h2>
				
				<h3>id: ${ article.getId() }</h3>
				<h3>Nombre: ${ article.getName() }</h3>
				<h3>Código: ${ article.getCode() }</h3>
				<h3>Precio: ${ article.getPrice() }</h3>

				<jsp:include page="../includes/footer.jsp"></jsp:include>
			</div>
		</div>
	</div>


</body>
</html>