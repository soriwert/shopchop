<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>    
<spring:url value="/resources" var="urlPublic" />
<spring:url value="/" var="url" />

<!doctype html>
<html>
<head>
<!-- Required meta tags -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<title>Shop Online</title>
</head>
<body>


	<jsp:include page="../includes/header.jsp"></jsp:include>
	<jsp:include page="../includes/menu.jsp"></jsp:include>

	<div class="container" style="margin-top: 30px">
		<div class="row">
			<div class="col-sm-12">
				<h1>Lista de artículos</h1>

				<table class="table">
					<c:forEach var="article" items="${ articles }">
						<tr>
							<td>${ article.id }</td>
							<td>${ article.code }</td>
							<td>${ article.name }</td>
							<td>${ article.price }</td>
							<td><a href="${ url }/articles/${article.id}">Ver</a>
							<td><a href="${ url }/articles/${article.id}/edit">Editar</a>
						</tr>
					</c:forEach>
				</table>

				<p>Página ${ page }</p>

				<a href="${ url }/articles/?page=1">1</a> - <a
					href="${ url }/articles/?page=2">2</a> - <a
					href="${ url }/articles/?page=3">3</a> - <a
					href="${ url }/articles/?page=4">4</a>
				<jsp:include page="../includes/footer.jsp"></jsp:include>
			</div>
		</div>
	</div>


</body>
</html>