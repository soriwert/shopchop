package com.rafacabeza.shop.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.rafacabeza.shop.model.Article;
import com.rafacabeza.shop.service.IArticleService;

@Controller
public class ArticleController {
	@Autowired
	private IArticleService articleService;
	
	@RequestMapping(value = "/articles", method = RequestMethod.GET)
	public String articles(Model model, @RequestParam(value = "page", required = false) Integer page) {
		System.out.println("pagina " + page);

		if (page == null) {
			page = 1;
		}

		ArrayList<Article> articles = articleService.all();

		model.addAttribute("articles", articles);
		model.addAttribute("page", page);

		return "article/index";
	}

	@RequestMapping(value = "/articles/{id}", method = RequestMethod.GET)
	public String article(Model model, @PathVariable("id") int id) {		
		Article article = articleService.find(id);
		model.addAttribute("article", article);
		return "article/show";
	}

	@RequestMapping(value = "/articles/{id}/edit", method = RequestMethod.GET)
	public String edit(Model model, @PathVariable("id") int id) {

		String name = "Teclado USB";
		double price = 12.5;
		String code = "KB01";

		model.addAttribute("id", id);
		model.addAttribute("name", name);
		model.addAttribute("code", code);
		model.addAttribute("price", price);

		return "article/edit";
	}

	@RequestMapping(value = "/articles/create", method = RequestMethod.GET)
	public String edit(Model model) {

		model.addAttribute("msg", "Creación de Articulo");

		return "article/article";
	}

	// POST store y update
	// @RequestMapping(value = "/articles/{id}", method = RequestMethod.POST)
	@PostMapping(value = "/articles/{id}")
	public String update(Model model, @PathVariable("id") int id) {

		model.addAttribute("msg", "Modificación de Articulo");
		return "article/article";
	}
}
