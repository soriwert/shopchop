package com.rafacabeza.shop;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App 
{
    public static void main( String[] args )
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
        System.out.println( "Contexto creado!" );        
        context.close();
        System.out.println( "Contexto cerrado!" );        
    }
}
