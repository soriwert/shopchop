package com.rafacabeza.shop.service;

import java.util.ArrayList;
import com.rafacabeza.shop.model.Article;

public interface IArticleService {
	ArrayList<Article> all();
	Article find(int id);
}
