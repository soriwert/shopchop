package com.rafacabeza.shop.service;

import java.util.ArrayList;
import java.util.Iterator;

import org.springframework.stereotype.Service;

import com.rafacabeza.shop.model.Article;

@Service
public class ArticleServiceImplementation implements IArticleService {
	private ArrayList<Article> articles = null;		

	public ArticleServiceImplementation() {
		System.out.println("creando servicio de articulos");
		articles = new ArrayList();		
		articles.add(new Article(1, "ART1", "articulo 1", 315));
		articles.add(new Article(2, "ART2", "articulo 2", 325));
		articles.add(new Article(3, "ART3", "articulo 3", 341));
		articles.add(new Article(4, "ART4", "articulo 4", 35));
		articles.add(new Article(5, "ART5", "articulo 5", 225));
	};
	@Override
	public ArrayList<Article> all() {
		return articles;
	}
	@Override
	public Article find(int id) {
		for (Iterator iterator = articles.iterator(); iterator.hasNext();) {
			Article article = (Article) iterator.next();
			System.out.println(article);
			if (article.getId() == id) {
				return article;
			}
		}
		return null;
	}
	
	

}
